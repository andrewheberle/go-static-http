package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"path/filepath"
	"time"

	"github.com/valyala/fasthttp"
)

var (
	serverRoot        = flag.String("root", ".", "Web root")
	listenAddress     = flag.String("listen", ":8080", "Listen address")
	allowIndex        = flag.Bool("allowindex", false, "Show directory indexes")
	enableCompression = flag.Bool("compression", false, "Enable HTTP compression")
	byteRange         = flag.Bool("byterange", false, "Enable byte range requests")
	readTimeout       = flag.Duration("readtimeout", 5*time.Second, "HTTP read timeout")
	writeTimeout      = flag.Duration("writetimeout", 10*time.Second, "HTTP write timeout")
	idleTimeout       = flag.Duration("idletimeout", 120*time.Second, "HTTP idle/keepalive timeout")
)

func healthHandler(ctx *fasthttp.RequestCtx) {
	ctx.SetContentType("application/json; charset=utf-8")
	ctx.SetStatusCode(fasthttp.StatusOK)
	if err := json.NewEncoder(ctx).Encode(map[string]bool{"ok": true}); err != nil {
		fmt.Printf("JSON encode error: %s\n", err)
	}
}

func main() {
	// parsing command line
	flag.Parse()

	// make serverRoot absolute
	if !filepath.IsAbs(*serverRoot) {
		sr, _ := filepath.Abs(*serverRoot)
		serverRoot = &sr
	}

	// Setup FS handler
	fs := &fasthttp.FS{
		Root:               *serverRoot,
		IndexNames:         []string{"index.html"},
		GenerateIndexPages: *allowIndex,
		Compress:           *enableCompression,
		AcceptByteRange:    *byteRange,
	}
	fsHandler := fs.NewRequestHandler()

	// Create RequestHandler
	requestHandler := func(ctx *fasthttp.RequestCtx) {
		switch string(ctx.Path()) {
		case "/healthz":
			healthHandler(ctx)
		default:
			fsHandler(ctx)
		}
	}

	// create server
	srv := fasthttp.Server{
		Handler:      requestHandler,
		ReadTimeout:  *readTimeout,
		WriteTimeout: *writeTimeout,
		IdleTimeout:  *idleTimeout,
	}

	// run server
	if err := srv.ListenAndServe(*listenAddress); err != nil {
		fmt.Printf("Error: %s\n", err)
	}
}
