# go-static-http

This is a basic web server for serving static files written in Go.

## Goal

A simple drop-in option to serve static file as fast as possible.

## Command Line

```sh
go-static-http [-listen <address>] [-root <web root>] [-allowindex]
[-compression] [-byterange] [-readtimeout <duration>]
[-writetimeout <duration>] [-idletimeout <duration>]
```

* `listen`: Listen address. Default: `:8080`
* `root`: Web reoot to serve files from. Default: `.`
* `allowindex`: Generate file listing index for directorories without an
`index.html`
* `compression`: Enable HTTP compression
* `byterange`: Allow byte range requests
* `readtimeout`: Read timeout for HTTP requests from the client. Default: `5s`
* `writetimeout`: Write timeout for HTTP responses to the client. Default: `10s`
* `idletimeout`: Timeout for idle/keepalive HTTP requests. Default: `120s`

## Logging

***Logging is currently disabled pending a solution that doesn't destroy
performance***

Logging is to stdout in Apache combined logging format.

By default only non-200 responses are logged, however this can be changed via
the `-logok` command line option.

Logging can be disabled by passing the `-nolog` command line option.

## TODO

* TLS/SSL support
* Make logging perform quickly
